//
//  MAAppManager.h
//  Modular App Hub
//
//  Created by Wills Ward on 6/24/15.
//  Copyright (c) 2015 Ward Development. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MAAppManager : NSObject

/**
 * gets singleton object.
 * @return singleton
 */
+ (MAAppManager*)sharedInstance;

/**
 *  Returns an array of apps in the modular hub
 *
 *  @return NSArray of MAApp objects
 */
- (NSArray*)apps;

@end
