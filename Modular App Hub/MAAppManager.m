//
//  MAAppManager.m
//  Modular App Hub
//
//  Created by Wills Ward on 6/24/15.
//  Copyright (c) 2015 Ward Development. All rights reserved.
//

#import "MAAppManager.h"
#import <objc/runtime.h>
#import <ModularAppCore/MAApp.h>

@implementation MAAppManager

static MAAppManager* SINGLETON = nil;

static bool isFirstAccess = YES;

#pragma mark - Public Method

+ (id)sharedInstance {
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
        isFirstAccess = NO;
        SINGLETON = [[super allocWithZone:NULL] init];
	});

	return SINGLETON;
}

#pragma mark - Life Cycle

+ (id)allocWithZone:(NSZone*)zone {
	return [self sharedInstance];
}

+ (id)copyWithZone:(struct _NSZone*)zone {
	return [self sharedInstance];
}

+ (id)mutableCopyWithZone:(struct _NSZone*)zone {
	return [self sharedInstance];
}

- (id)copy {
	return [[MAAppManager alloc] init];
}

- (id)mutableCopy {
	return [[MAAppManager alloc] init];
}

- (id)init {
	if (SINGLETON) {
		return SINGLETON;
	}
	if (isFirstAccess) {
		[self doesNotRecognizeSelector:_cmd];
	}
	self = [super init];
	return self;
}

- (NSArray*)apps {
	NSMutableArray* modules = [NSMutableArray new];
	Class* classes = NULL;
	int numClasses = objc_getClassList(NULL, 0);
	if (numClasses > 0) {
		classes = (Class*) malloc(sizeof(Class) * numClasses);
		numClasses = objc_getClassList(classes, numClasses);
		for (int index = 0; index < numClasses; index++) {
			Class nextClass = classes[index];
			if (class_conformsToProtocol(nextClass, @protocol(MAApp))) {
				id<MAApp> instantiatedClass = nil;
				instantiatedClass = [[nextClass alloc] init];
				[modules addObject:instantiatedClass];
			}
		}
		free(classes);
	}

	return [modules copy];
}

@end
