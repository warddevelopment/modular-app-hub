//
//  AppDelegate.h
//  Modular App Hub
//
//  Created by Wills Ward on 6/24/15.
//  Copyright (c) 2015 Ward Development. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

