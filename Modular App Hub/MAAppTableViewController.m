//
//  MAAppTableViewController.m
//  Modular App Hub
//
//  Created by Wills Ward on 6/24/15.
//  Copyright (c) 2015 Ward Development. All rights reserved.
//

#import "MAAppTableViewController.h"
#import <ModularAppCore/MAApp.h>
#import "MAAppManager.h"

#define APP_CELL @"APP_CELL"

@interface MAAppTableViewController ()

@property MAAppManager* appManager;

@end

@implementation MAAppTableViewController

- (void)viewDidLoad {
	[super viewDidLoad];

	_appManager = [MAAppManager sharedInstance];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
	// Return the number of sections.
	return 1;
}

- (NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
	// Return the number of rows in the section.
	return _appManager.apps.count;
}

- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {
	UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:APP_CELL forIndexPath:indexPath];

	id<MAApp> app = _appManager.apps[indexPath.row];

	cell.textLabel.text = [app name];

	return cell;
}

- (void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath {
	id<MAApp> app = _appManager.apps[indexPath.row];
	[app invoke];
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
