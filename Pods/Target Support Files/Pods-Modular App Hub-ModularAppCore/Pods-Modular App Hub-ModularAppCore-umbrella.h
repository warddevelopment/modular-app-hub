#import <UIKit/UIKit.h>

#import "MAApp.h"
#import "MAModel.h"

FOUNDATION_EXPORT double ModularAppCoreVersionNumber;
FOUNDATION_EXPORT const unsigned char ModularAppCoreVersionString[];

